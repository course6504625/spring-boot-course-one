package tech.escalab.demo.spring_boot.application.use_case.person;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import tech.escalab.demo.spring_boot.domain.port.person.PersonPort;

import java.util.UUID;

@AllArgsConstructor
@Component
public class DeletePersonUseCase {

    private PersonPort personPort;

    public void execute(UUID personUuid) {

        personPort.findByUuid(personUuid);

        personPort.deleteByUuid(personUuid);
    }
}
