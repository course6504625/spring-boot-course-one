package tech.escalab.demo.spring_boot.application.use_case.person;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.domain.port.person.PersonPort;

import java.util.UUID;

@AllArgsConstructor
@Component
public class UpdatePersonUseCase {

    private PersonPort personPort;

    public void execute(UUID personUuid, Person person) {

        var personFound = personPort.findByUuid(personUuid);

        personPort.update(person, personFound);
    }
}
