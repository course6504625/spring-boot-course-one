package tech.escalab.demo.spring_boot.application.use_case.person;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.domain.port.person.PersonPort;

@AllArgsConstructor
@Component
public class SavePersonUseCase {

    private PersonPort personPort;

    public Person execute(Person person) {

        return personPort.save(person);
    }
}
