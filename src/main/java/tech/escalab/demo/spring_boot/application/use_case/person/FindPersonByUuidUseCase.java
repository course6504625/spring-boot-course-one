package tech.escalab.demo.spring_boot.application.use_case.person;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.domain.port.person.PersonPort;

import java.util.UUID;

@AllArgsConstructor
@Component
public class FindPersonByUuidUseCase {

    private PersonPort personPort;

    public Person execute(UUID personUuid) {

        return personPort.findByUuid(personUuid);
    }
}
