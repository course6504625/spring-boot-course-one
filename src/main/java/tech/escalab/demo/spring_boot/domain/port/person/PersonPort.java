package tech.escalab.demo.spring_boot.domain.port.person;

import tech.escalab.demo.spring_boot.domain.model.person.Person;

import java.util.UUID;

public interface PersonPort {

    void deleteByUuid(UUID uuid);

    Person findByUuid(UUID uuid);

    Person save(Person person);

    Person update(Person personToUpdate, Person personFound);
}
