package tech.escalab.demo.spring_boot.domain.service.person;

import java.util.List;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.request.PersonRequestDto;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.response.PersonResponseDto;

public interface PersonService {

    List<PersonResponseDto> findAll();

    PersonResponseDto save(PersonRequestDto request);
}
