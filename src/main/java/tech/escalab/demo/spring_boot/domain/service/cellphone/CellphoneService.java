package tech.escalab.demo.spring_boot.domain.service.cellphone;

import tech.escalab.demo.spring_boot.domain.model.cellphone.Cellphone;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.cellphone.request.CellphoneRequestDto;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.cellphone.response.CellphoneResponseDto;

public interface CellphoneService {

    CellphoneResponseDto addToPerson(Person person, CellphoneRequestDto request);

    Cellphone save(Cellphone cellphone);
}
