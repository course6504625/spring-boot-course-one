package tech.escalab.demo.spring_boot.domain.exception.general;

import tech.escalab.demo.spring_boot.domain.exception.base.DomainException;
import tech.escalab.demo.spring_boot.domain.exception.base.DomainExceptionCode;

public class NotFoundException extends DomainException {

    public NotFoundException(DomainExceptionCode code) {
        super(code);
    }
}
