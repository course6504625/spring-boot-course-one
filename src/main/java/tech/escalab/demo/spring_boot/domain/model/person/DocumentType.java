package tech.escalab.demo.spring_boot.domain.model.person;

public enum DocumentType {

    CC,
    TI,
    PS
}
