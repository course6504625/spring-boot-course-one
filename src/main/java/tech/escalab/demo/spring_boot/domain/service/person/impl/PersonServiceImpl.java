package tech.escalab.demo.spring_boot.domain.service.person.impl;

import java.util.List;
import java.util.Objects;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.person.PersonRepositoryJpa;
import tech.escalab.demo.spring_boot.domain.service.person.PersonService;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.request.PersonRequestDto;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.response.PersonResponseDto;
import tech.escalab.demo.spring_boot.infrastructure.api.mapper.person.PersonMapper;
import tech.escalab.demo.spring_boot.domain.service.cellphone.CellphoneService;

@AllArgsConstructor
@Service
public class PersonServiceImpl implements PersonService {

    private CellphoneService cellphoneService;
    private PersonRepositoryJpa personRepositoryJpa;
    private PersonMapper personMapper;

    @Override
    public List<PersonResponseDto> findAll() {

        /*
        return personRepositoryJpa.findAll().stream()
                .map(person -> personMapper.toResponseDto(person))
                .toList();

         */
        return null;
    }

    @Override
    public PersonResponseDto save(PersonRequestDto request) {

        //var personSaved = personRepositoryJpa.save(personMapper.toPerson(request));

        if (Objects.nonNull(request.getCellphone())) {
            //cellphoneService.addToPerson(personSaved, request.getCellphone());
        }

        //return personMapper.toResponseDto(personSaved);
        return null;
    }
}
