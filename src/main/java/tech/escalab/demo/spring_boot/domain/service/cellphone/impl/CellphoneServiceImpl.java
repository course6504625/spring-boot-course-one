package tech.escalab.demo.spring_boot.domain.service.cellphone.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.cellphone.CellphoneRepositoryJpa;
import tech.escalab.demo.spring_boot.domain.model.cellphone.Cellphone;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.cellphone.request.CellphoneRequestDto;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.cellphone.response.CellphoneResponseDto;
import tech.escalab.demo.spring_boot.domain.service.cellphone.CellphoneService;

@AllArgsConstructor
@Service
public class CellphoneServiceImpl implements CellphoneService {

    private CellphoneRepositoryJpa cellphoneRepositoryJpa;

    @Override
    public CellphoneResponseDto addToPerson(Person person, CellphoneRequestDto request) {

        var cellphoneSaved = cellphoneRepositoryJpa.save(
                request.toEntity(person)
        );

        return CellphoneResponseDto.toThis(cellphoneSaved);
    }

    @Override
    public Cellphone save(Cellphone cellphone) {

        return cellphoneRepositoryJpa.save(cellphone);
    }
}
