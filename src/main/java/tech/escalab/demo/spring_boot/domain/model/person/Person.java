package tech.escalab.demo.spring_boot.domain.model.person;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class Person {

    private UUID uuid;

    private String name;

    private String lastName;

    private Integer age;

    private DocumentType documentType;

    private String code;

    private Boolean isDeleted;

    private LocalDateTime deletedAt;

    //private Cellphone cellphone;

    //private List<Address> addresses;

    //private List<Course> courses;
}
