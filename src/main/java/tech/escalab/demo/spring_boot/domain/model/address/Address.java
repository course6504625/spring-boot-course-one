package tech.escalab.demo.spring_boot.domain.model.address;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "addresses")
@Getter
@Setter
public class Address {

    @Id
    private UUID uuid = UUID.randomUUID();

    private String address;

    //@ManyToOne(fetch = FetchType.LAZY)
    //private Person person;
}
