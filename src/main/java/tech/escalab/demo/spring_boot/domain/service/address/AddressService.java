package tech.escalab.demo.spring_boot.domain.service.address;

import tech.escalab.demo.spring_boot.domain.model.address.Address;
import tech.escalab.demo.spring_boot.domain.model.person.Person;

import java.util.List;

public interface AddressService {

    void addAllToPerson(Person person, List<Address> addresses);
}
