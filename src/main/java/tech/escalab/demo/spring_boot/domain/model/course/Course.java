package tech.escalab.demo.spring_boot.domain.model.course;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "courses")
@Getter
@Setter
public class Course {

    @Id
    private UUID uuid = UUID.randomUUID();

    private String name;

    //@ManyToMany(mappedBy = "courses")
    //private Set<Person> people = new HashSet<>();
}
