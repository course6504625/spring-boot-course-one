package tech.escalab.demo.spring_boot.domain.service.number;

import java.util.Random;
import org.springframework.stereotype.Service;

@Service
public class NumberService {

    public Integer getNumber() {

        var random = new Random();

        return random.nextInt(30);
    }

}
