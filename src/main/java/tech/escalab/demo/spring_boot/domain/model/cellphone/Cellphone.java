package tech.escalab.demo.spring_boot.domain.model.cellphone;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "cellphones")
@Getter
@Setter
public class Cellphone {

    @Id
    private UUID uuid = UUID.randomUUID();

    private String number;

    //@OneToOne(fetch = FetchType.LAZY)
    //private Person person;
}
