package tech.escalab.demo.spring_boot.domain.service.address.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.address.AddressRepositoryJpa;
import tech.escalab.demo.spring_boot.domain.model.address.Address;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.domain.service.address.AddressService;

import java.util.List;

@AllArgsConstructor
@Service
public class AddressServiceImpl implements AddressService {

    private AddressRepositoryJpa addressRepositoryJpa;

    @Override
    public void addAllToPerson(Person person, List<Address> addresses) {

        //addresses.forEach(address -> address.setPerson(person));

        addressRepositoryJpa.saveAll(addresses);
    }
}
