package tech.escalab.demo.spring_boot.infrastructure.api.mapper.person;

import org.springframework.stereotype.Component;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.request.PersonRequestDto;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.response.PersonResponseDto;

import java.util.Objects;

@Component
public class PersonMapper {

    public void update(Person personSource, Person personTarget) {
        /*
        if (Objects.nonNull(personSource.getName())) {
            personTarget.setName(personSource.getName());
        }

        if (Objects.nonNull(personSource.getLastName())) {
            personTarget.setLastName(personSource.getLastName());
        }

        if (Objects.nonNull(personSource.getAge())) {
            personTarget.setAge(personSource.getAge());
        }

        if (Objects.nonNull(personSource.getDocumentType())) {
            personTarget.setDocumentType(personSource.getDocumentType());
        }

        if (Objects.nonNull(personSource.getDocumentType())) {
            personTarget.setDocumentType(personSource.getDocumentType());
        }
         */
    }

    public PersonResponseDto toResponseDto(Person person) {
        /*
        if (person == null) {
            return null;
        }

        var personResponseDto = new PersonResponseDto();

        if (Objects.nonNull(person.getUuid())) {
            personResponseDto.setUuid(person.getUuid());
        }

        if (Objects.nonNull(person.getName())) {
            personResponseDto.setName(person.getName());
        }

        if (Objects.nonNull(person.getDocumentType())) {
            personResponseDto.setDocumentType(person.getDocumentType());
        }

        return personResponseDto;
         */
        return null;
    }

    public Person toPerson(PersonRequestDto request) {
        /*
        if (request == null) {
            return null;
        }

        var person = new Person();

        if (Objects.nonNull(request.getName())) {
            person.setName(request.getName());
        }

        if (Objects.nonNull(request.getLastName())) {
            person.setLastName(request.getLastName());
        }

        if (Objects.nonNull(request.getAge())) {
            person.setAge(request.getAge());
        }

        if (Objects.nonNull(request.getDocumentType())) {
            person.setDocumentType(request.getDocumentType());
        }

        return person;
         */
        return null;
    }
}
