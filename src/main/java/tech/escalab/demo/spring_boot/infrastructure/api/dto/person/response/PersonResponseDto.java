package tech.escalab.demo.spring_boot.infrastructure.api.dto.person.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tech.escalab.demo.spring_boot.domain.model.person.DocumentType;

import java.util.UUID;

@Getter
@Setter
@ToString
public class PersonResponseDto {

    private Integer age;

    private DocumentType documentType = DocumentType.CC;

    private String name;

    private UUID uuid;

}
