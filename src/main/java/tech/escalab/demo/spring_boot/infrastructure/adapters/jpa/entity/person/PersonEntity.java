package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.person;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import tech.escalab.demo.spring_boot.domain.model.person.DocumentType;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.person.listener.PersonListener;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@EntityListeners({PersonListener.class})
@Table(name = "people")
@SQLDelete(sql = "UPDATE people SET is_deleted=true, deleted_at=now() WHERE uuid=?")
@Where(clause = "is_deleted is false")
@Getter
@Setter
public class PersonEntity {

    @Id
    private UUID uuid = UUID.randomUUID();

    @Size(min = 5, max = 10, message = "El tamaño del nombre no es correcto")
    private String name;

    @NotNull
    private String lastName;

    @NotNull
    private Integer age;

    @Column(length = 100)
    @Enumerated(EnumType.STRING)
    private DocumentType documentType = DocumentType.CC;

    private String code;

    private Boolean isDeleted = Boolean.FALSE;

    private LocalDateTime deletedAt;

    //@OneToOne(mappedBy = "person", fetch = FetchType.LAZY)
    //private Cellphone cellphone;

    //@OneToMany(mappedBy = "person")
    //private Set<Address> addresses;

    //@ManyToMany
    /*
    @JoinTable(
            name = "person_course_liked",
            joinColumns = @JoinColumn(name = "person_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id")
    )
     */
    //private Set<Course> courses = new HashSet<>();
}
