package tech.escalab.demo.spring_boot.infrastructure.api.controller.authentication;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.authentication.request.SignInRequest;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.authentication.request.SignUpRequest;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.authentication.response.AuthenticationResponse;
import tech.escalab.demo.spring_boot.infrastructure.authentication.authentication_provider.JwtAuthenticationProvider;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/auth")
public class AuthenticationController {

    private JwtAuthenticationProvider jwtAuthenticationProvider;

    @PostMapping("/signup")
    public AuthenticationResponse signup(@RequestBody SignUpRequest request) {

        return jwtAuthenticationProvider.signUp(request);
    }

    @PostMapping("/signin")
    public AuthenticationResponse signin(@RequestBody SignInRequest request) {

        return jwtAuthenticationProvider.signIn(request);
    }
}
