package tech.escalab.demo.spring_boot.infrastructure.api.mapper.person;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.request.PersonRequestDto;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.response.PersonResponseDto;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface PersonApiMapper {

    Person toDomain(PersonRequestDto request);

    PersonResponseDto toResponse(Person person);
}
