package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.cellphone;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.escalab.demo.spring_boot.domain.model.cellphone.Cellphone;

import java.util.UUID;

@Repository
public interface CellphoneRepositoryJpa extends CrudRepository<Cellphone, UUID> {
}
