package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.user_authentication;

public enum Role {

    USER,
    ADMIN
}
