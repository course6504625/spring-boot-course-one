package tech.escalab.demo.spring_boot.infrastructure.api.dto.cellphone.response;

import lombok.Getter;
import lombok.Setter;
import tech.escalab.demo.spring_boot.domain.model.cellphone.Cellphone;

import java.util.UUID;

@Getter
@Setter
public class CellphoneResponseDto {

    private String number;

    private UUID uuid;

    public static CellphoneResponseDto toThis(Cellphone cellphone) {

        var response = new CellphoneResponseDto();
        response.setNumber(cellphone.getNumber());
        response.setUuid(cellphone.getUuid());

        return response;
    }
}
