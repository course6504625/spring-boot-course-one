package tech.escalab.demo.spring_boot.infrastructure.api.controller.person;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.escalab.demo.spring_boot.application.use_case.person.DeletePersonUseCase;
import tech.escalab.demo.spring_boot.application.use_case.person.FindPersonByUuidUseCase;
import tech.escalab.demo.spring_boot.application.use_case.person.SavePersonUseCase;
import tech.escalab.demo.spring_boot.application.use_case.person.UpdatePersonUseCase;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.request.PersonRequestDto;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.response.PersonResponseDto;
import tech.escalab.demo.spring_boot.domain.service.person.PersonService;
import tech.escalab.demo.spring_boot.infrastructure.api.mapper.person.PersonApiMapper;
import tech.escalab.demo.spring_boot.infrastructure.api.swagger.person.PersonSwagger;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/people")
public class PersonController implements PersonSwagger {

    private DeletePersonUseCase deletePersonUseCase;
    private FindPersonByUuidUseCase findPersonByUuidUseCase;
    private PersonService personService;
    private SavePersonUseCase savePersonUseCase;
    private UpdatePersonUseCase updatePersonUseCase;
    private PersonApiMapper personApiMapper;

    @Override
    @GetMapping
    public List<PersonResponseDto> findAll() {

        return personService.findAll();
    }

    @Override
    @GetMapping("/{uuid}")
    public PersonResponseDto findById(
            @PathVariable UUID uuid
    ) {

        return personApiMapper.toResponse(
                findPersonByUuidUseCase.execute(uuid)
        );
    }

    @Override
    @PostMapping
    public PersonResponseDto save(
            @Valid @RequestBody PersonRequestDto request
    ) {

        var savedPerson = savePersonUseCase.execute(
                personApiMapper.toDomain(request)
        );

        return personApiMapper.toResponse(savedPerson);
    }

    @Override
    @PutMapping("/{uuid}")
    public void update(
            @PathVariable UUID uuid,
            @RequestBody PersonRequestDto request
    ) {

        updatePersonUseCase.execute(
                uuid,
                personApiMapper.toDomain(request)
        );
    }

    @Override
    @DeleteMapping("/{uuid}")
    public void delete(@PathVariable UUID uuid) {

        deletePersonUseCase.execute(uuid);
    }
}
