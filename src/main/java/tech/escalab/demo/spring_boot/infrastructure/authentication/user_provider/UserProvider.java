package tech.escalab.demo.spring_boot.infrastructure.authentication.user_provider;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import tech.escalab.demo.spring_boot.domain.exception.base.DomainExceptionCode;
import tech.escalab.demo.spring_boot.domain.exception.general.NotFoundException;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.user_authentication.UserAuthenticationRepositoryJpa;

@AllArgsConstructor
@Component
public class UserProvider {

    private UserAuthenticationRepositoryJpa userAuthenticationRepositoryJpa;

    public UserDetailsService getUserDetailsService() {

        return new UserDetailsService() {

            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

                return userAuthenticationRepositoryJpa.findByEmail(username)
                        .orElseThrow( () -> new NotFoundException(DomainExceptionCode.PERSON_NOT_FOUND) );
            }
        };
    }
}
