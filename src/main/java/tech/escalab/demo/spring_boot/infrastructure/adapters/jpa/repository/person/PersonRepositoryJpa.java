package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.person;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.person.PersonEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PersonRepositoryJpa extends CrudRepository<PersonEntity, UUID> {

    Optional<PersonEntity> findFirstByCodeNotNullOrderByCodeDesc();

    List<PersonEntity> findAll();

    List<PersonEntity> findAllByName(String name);

    Optional<PersonEntity> findByName(String name);

    Optional<PersonEntity> findByLastName(String lastName);
}
