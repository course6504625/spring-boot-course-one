package tech.escalab.demo.spring_boot.infrastructure.api.swagger.person;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.request.PersonRequestDto;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.response.PersonResponseDto;

import java.util.List;
import java.util.UUID;

@Tag(name = "Personas", description = "API de personas")
public interface PersonSwagger {

    @Operation(
            summary = "Listar personas",
            description = "Endpoint que nos ayuda a listar todas las personas de la base de datos"
    )
    @ApiResponse(
            responseCode = "200", content = { @Content(schema = @Schema(implementation = PersonResponseDto.class), mediaType = "application/json") }
    )
    List<PersonResponseDto> findAll();

    @Operation(
            summary = "Buscar persona",
            description = "Endpoint que nos ayuda encontrar una persona por su uuid"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = PersonResponseDto.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema(implementation = PersonResponseDto.class), mediaType = "application/json") })
    })
    PersonResponseDto findById(UUID uuid);

    @Operation(
            summary = "Guardar persona",
            description = "Endpoint que nos ayuda guardar una persona"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = PersonResponseDto.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema(implementation = PersonResponseDto.class), mediaType = "application/json") })
    })
    PersonResponseDto save(PersonRequestDto request);

    @Operation(
            summary = "Actualizar una persona",
            description = "Endpoint que nos ayuda a actualizar una persona por medio de su uuid y la informacion a modificar"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = PersonResponseDto.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema(implementation = PersonResponseDto.class), mediaType = "application/json") })
    })
    void update(UUID uui, PersonRequestDto request);

    void delete(UUID uuid);
}
