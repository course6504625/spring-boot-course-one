package tech.escalab.demo.spring_boot.infrastructure.authentication.authentication_provider;

import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.user_authentication.Role;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.user_authentication.UserAuthentication;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.user_authentication.UserAuthenticationRepositoryJpa;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.authentication.request.SignUpRequest;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.authentication.request.SignInRequest;
import tech.escalab.demo.spring_boot.infrastructure.api.dto.authentication.response.AuthenticationResponse;
import tech.escalab.demo.spring_boot.infrastructure.authentication.jwt_provider.JwtProvider;

@AllArgsConstructor
@Component
public class JwtAuthenticationProvider {

    private AuthenticationManager authenticationManager;
    private JwtProvider jwtProvider;
    private PasswordEncoder passwordEncoder;
    private UserAuthenticationRepositoryJpa userAuthenticationRepositoryJpa;

    public AuthenticationResponse signUp(SignUpRequest request) {

        var user = new UserAuthentication();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setEmail(request.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRole(Role.USER);

        userAuthenticationRepositoryJpa.save(user);

        var jwt = jwtProvider.generateToken(user);

        return AuthenticationResponse.builder().token(jwt).build();
    }

    public AuthenticationResponse signIn(SignInRequest request) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword())
        );

        var user = userAuthenticationRepositoryJpa.findByEmail(request.getEmail())
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password"));

        var jwt = jwtProvider.generateToken(user);

        return AuthenticationResponse.builder().token(jwt).build();
    }
}
