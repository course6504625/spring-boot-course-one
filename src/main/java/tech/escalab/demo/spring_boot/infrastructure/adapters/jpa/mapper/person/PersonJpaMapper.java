package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.mapper.person;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.person.PersonEntity;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface PersonJpaMapper {

    PersonEntity toEntity(Person person);

    Person toDomain(PersonEntity personEntity);

    void updateDomain(Person personToUpdate, @MappingTarget Person personFound);
}
