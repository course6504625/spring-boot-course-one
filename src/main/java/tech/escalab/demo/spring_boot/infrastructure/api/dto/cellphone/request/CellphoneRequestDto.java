package tech.escalab.demo.spring_boot.infrastructure.api.dto.cellphone.request;

import lombok.Getter;
import lombok.Setter;
import tech.escalab.demo.spring_boot.domain.model.cellphone.Cellphone;
import tech.escalab.demo.spring_boot.domain.model.person.Person;

@Getter
@Setter
public class CellphoneRequestDto {

    private String number;

    public Cellphone toEntity(Person person) {

        var cellphone = new Cellphone();
        cellphone.setNumber(this.number);
        //cellphone.setPerson(person);

        return cellphone;
    }
}
