package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.adapter.person;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import tech.escalab.demo.spring_boot.domain.exception.base.DomainExceptionCode;
import tech.escalab.demo.spring_boot.domain.exception.general.NotFoundException;
import tech.escalab.demo.spring_boot.domain.model.person.Person;
import tech.escalab.demo.spring_boot.domain.port.person.PersonPort;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.mapper.person.PersonJpaMapper;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.person.PersonRepositoryJpa;

import java.util.UUID;

@AllArgsConstructor
@Component
public class PersonPortAdapter implements PersonPort {

    private PersonJpaMapper personJpaMapper;
    private PersonRepositoryJpa personRepositoryJpa;

    @Override
    public void deleteByUuid(UUID uuid) {

        personRepositoryJpa.deleteById(uuid);
    }

    @Override
    public Person findByUuid(UUID uuid) {

        var personEntity = personRepositoryJpa.findById(uuid).orElseThrow(
                () -> new NotFoundException(DomainExceptionCode.PERSON_NOT_FOUND)
        );

        return personJpaMapper.toDomain(personEntity);
    }

    @Override
    public Person save(Person person) {

        var savedPerson = personRepositoryJpa.save(
                personJpaMapper.toEntity(person)
        );

        return personJpaMapper.toDomain(savedPerson);
    }

    @Override
    public Person update(Person personToUpdate, Person personFound) {

        personJpaMapper.updateDomain(personToUpdate, personFound);

        var personEntity = personJpaMapper.toEntity(personFound);

        return personJpaMapper.toDomain(
                personRepositoryJpa.save(personEntity)
        );
    }
}
