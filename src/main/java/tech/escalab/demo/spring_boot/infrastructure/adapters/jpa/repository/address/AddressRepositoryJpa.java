package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.address;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.escalab.demo.spring_boot.domain.model.address.Address;

import java.util.UUID;

@Repository
public interface AddressRepositoryJpa extends CrudRepository<Address, UUID> {
}
