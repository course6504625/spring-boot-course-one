package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.user_authentication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.user_authentication.UserAuthentication;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserAuthenticationRepositoryJpa extends JpaRepository<UserAuthentication, UUID> {

    Optional<UserAuthentication> findByEmail(String email);
}
