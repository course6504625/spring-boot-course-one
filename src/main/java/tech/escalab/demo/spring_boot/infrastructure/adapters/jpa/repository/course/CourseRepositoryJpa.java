package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.course;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.escalab.demo.spring_boot.domain.model.course.Course;

import java.util.UUID;

@Repository
public interface CourseRepositoryJpa extends CrudRepository<Course, UUID> {
}
