package tech.escalab.demo.spring_boot.infrastructure.api.controller.number;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.escalab.demo.spring_boot.domain.service.number.NumberService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("api/v1/numbers")
public class NumberController {

    private NumberService numberService;

    public NumberController(NumberService numberService) {
        this.numberService = numberService;
    }

    //@CrossOrigin(origins = "*")
    @GetMapping("/")
    public Integer getNumber() {
        return numberService.getNumber();
    }

    //@CrossOrigin("http://example.com")
    @GetMapping("/{id}")
    public Integer getNumberTwo() {
        return numberService.getNumber();
    }

}
