package tech.escalab.demo.spring_boot.infrastructure.api.controller.value;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/value")
class ValueController {

    @Value("${app.customer.name:Jose}")
    private String name;

    public ValueController() {
    }

    @GetMapping
    public String getNameValue() {

        return name;
    }
}
