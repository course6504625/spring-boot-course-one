package tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.person.listener;

import jakarta.persistence.PrePersist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.entity.person.PersonEntity;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.person.PersonRepositoryJpa;
import java.util.Objects;

@Component
public class PersonListener {

    private static PersonRepositoryJpa repository;

    @Autowired
    public void setPersonRepositoryJpa(PersonRepositoryJpa repository) {
        PersonListener.repository = repository;
    }


    @PrePersist
    public void onPreInsert(PersonEntity personEntity) {

        if (personEntity.getCode() == null) {

            var codeToSave = 0L;

            var personFound = repository.findFirstByCodeNotNullOrderByCodeDesc();

            if (personFound.isPresent() && Objects.nonNull(personFound.get().getCode())) {

                codeToSave = Long.parseLong(personFound.get().getCode().split("-")[1]);
            }

            codeToSave++;

            personEntity.setCode("PER-"+ codeToSave);
        }
    }
}
