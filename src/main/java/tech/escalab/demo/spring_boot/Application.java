package tech.escalab.demo.spring_boot;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tech.escalab.demo.spring_boot.infrastructure.adapters.jpa.repository.course.CourseRepositoryJpa;
import tech.escalab.demo.spring_boot.domain.service.address.AddressService;
import tech.escalab.demo.spring_boot.domain.service.cellphone.CellphoneService;
import tech.escalab.demo.spring_boot.domain.service.person.PersonService;

@SpringBootApplication
public class Application implements CommandLineRunner {

	@Autowired
	private PersonService personService;

	@Autowired
	private CellphoneService cellphoneService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private CourseRepositoryJpa courseRepositoryJpa;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Transactional
	@Override
	public void run(String... args) throws Exception {

		/*
		// Guardar personas
		var person = new Person();
		person.setName("Jaime");
		person.setLastName("Morales");
		person.setAge(30);

		personService.save(person);

		var person2 = new Person();
		person2.setName("Camilo");
		person2.setLastName("Contreras");
		person2.setAge(28);

		personService.save(person2);

		personService.findAll().forEach(System.out::println);

		/*
		// Eliminar persona
		var personFound = personService.findByName("Jaime");

		personService.delete(personFound.getUuid());

		// Encontrar una persona por su nombre
		personFound = personService.findByName("Camilo");

		// Registrar el numero telefonico de una persona (relacion: uno a uno)
		var cellphone = new Cellphone();
		cellphone.setNumber("34324329458");

		cellphoneService.addToPerson(personFound, cellphone);

		// Registrar varias direcciones de una persona (relacion: uno a muchos)
		var firstAddress = new Address();
		firstAddress.setAddress("Direccion X No. 13-56");

		var secondAddress = new Address();
		secondAddress.setAddress("Direccion Y No. 67-12");

		addressService.addAllToPerson(personFound, Arrays.asList(firstAddress, secondAddress));

		/*
		// Crear un curso para relacionarlo con persona
		var course = new Course();
		course.setName("Spring Boot");

		var courseSaved = courseRepositoryJpa.save(course);

		personFound.getCourses().add(courseSaved);

		personRepositoryJpa.save(personFound);
		 */
	}
}
