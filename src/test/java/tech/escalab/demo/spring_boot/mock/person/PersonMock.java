package tech.escalab.demo.spring_boot.mock.person;

import tech.escalab.demo.spring_boot.domain.model.person.Person;

public class PersonMock {

    public static Person build() {

        var person = new Person();
        person.setName("Jaime");

        return person;
    }
}
