package tech.escalab.demo.spring_boot.mock.person;

import tech.escalab.demo.spring_boot.infrastructure.api.dto.person.response.PersonResponseDto;

public class PersonResponseDtoMock {

    public static PersonResponseDto build() {

        var personResponse = new PersonResponseDto();
        personResponse.setName("Jaime");

        return personResponse;
    }
}
